use std::collections::BTreeMap;
use std::fmt::Debug;

// pub struct RAM(Box<[u8; u32::max_value() as usize + 1]>);
//
// impl Default for RAM {
//     fn default() -> Self {
//         RAM(Box::new([0; u32::max_value() as usize + 1]))
//     }
// }
//
// Let's not allocate 4GB at once...

#[derive(Debug)]
pub struct RAM(BTreeMap<u32, u8>);

impl Default for RAM {
    fn default() -> Self {
        RAM(BTreeMap::new())
    }
}

impl RAM {
    pub fn get_byte(&self, addr: u32) -> u8 {
        self.0.get(&addr).copied().unwrap_or(0)
    }

    pub fn get_word(&self, addr: u32) -> i32 {
        let b1 = self.get_byte(addr);
        let b2 = self.get_byte(addr + 1);
        let b3 = self.get_byte(addr + 2);
        let b4 = self.get_byte(addr + 3);

        i32::from_be_bytes([b1, b2, b3, b4])
    }

    pub fn set_byte(&mut self, addr: u32, data: u8) {
        self.0.insert(addr, data);
    }

    pub fn set_word(&mut self, addr: u32, data: i32) {
        let bytes = data.to_be_bytes();
        for (i, byte) in bytes.iter().enumerate() {
            self.set_byte(addr + i as u32, *byte);
        }
    }
}

// impl Debug for RAM {
//     fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
//         write!(f, "RAM {{ .. }}")
//     }
// }
