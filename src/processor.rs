use crate::alu::{ALUConfiguration, ALU};
use crate::micro::bbus::BBusConfiguration;
use crate::micro::cbus::{CBusConfiguration, CBusFlag};
use crate::micro::control_store::ControlStore;
use crate::ram::RAM;
use bitfield::fmt::Debug;

#[derive(Debug)]
pub enum MemoryOp {
    Fetch(u32),
    Read(u32),
    FetchRead(u32, u32),
    None,
}

impl Default for MemoryOp {
    fn default() -> Self {
        MemoryOp::None
    }
}

#[derive(Debug, Default)]
pub struct Mic1 {
    pub(crate) ram: RAM,
    last_mem_op:    MemoryOp,
    pub(crate) mar: u32,
    pub(crate) mdr: i32,
    pub(crate) pc:  i32,
    mbr:            u8,
    pub(crate) sp:  i32,
    pub(crate) lv:  i32,
    pub(crate) cpp: i32,
    pub(crate) tos: i32,
    pub(crate) opc: i32,
    pub(crate) h:   i32,
    pub(crate) mpc: u16,
    alu:            ALU,
}

impl Mic1 {
    pub fn new() -> Mic1 {
        let mut x = Mic1::default();

        x.lv = 0x150;
        x.sp = 0x150;
        x.cpp = 0x100;
        x.pc = -1;
        x
    }

    pub fn load_program(&mut self, program: Vec<u8>) {
        for (i, x) in program.iter().copied().enumerate() {
            self.ram.set_byte(i as u32, x);
        }
    }

    pub(crate) fn mbru(&self) -> u32 {
        self.mbr as u32
    }

    pub(crate) fn mbr(&self) -> i32 {
        i8::from_be_bytes([self.mbr]) as i32
    }

    pub fn clock(&mut self) -> bool {
        // delta w

        if self.pc == 0x100 {
            return true;
        }

        // println!("Executing MIR: {}", self.mpc);
        let mir = ControlStore::get_instr(self.mpc);

        // delta x

        let b_bus_conf = BBusConfiguration::from(mir.b());
        let b = match b_bus_conf {
            BBusConfiguration::MDR => self.mdr,
            BBusConfiguration::PC => self.pc as i32,
            BBusConfiguration::MBR => self.mbr(),
            BBusConfiguration::MBRU => self.mbru() as i32,
            BBusConfiguration::SP => self.sp as i32,
            BBusConfiguration::LV => self.lv as i32,
            BBusConfiguration::CPP => self.cpp,
            BBusConfiguration::TOS => self.tos,
            BBusConfiguration::OPC => self.opc,
            BBusConfiguration::None => 0,
        };

        // println!("Value on B: {}", b);

        // delta y

        let c = self.alu.calculate(ALUConfiguration::from(&mir), self.h, b);

        // println!("Value on C: {}", c);

        // delta z

        for register in CBusConfiguration::from(&mir).0 {
            match register {
                CBusFlag::H => self.h = c,
                CBusFlag::OPC => self.opc = c,
                CBusFlag::TOS => self.tos = c,
                CBusFlag::CPP => self.cpp = c,
                CBusFlag::LV => self.lv = c,
                CBusFlag::SP => self.sp = c,
                CBusFlag::PC => self.pc = c,
                CBusFlag::MDR => self.mdr = c,
                CBusFlag::MAR => self.mar = c as u32,
            }
        }

        if mir.write() {
            self.ram.set_word(self.mar * 4, self.mdr);
        }

        match self.last_mem_op {
            MemoryOp::Fetch(addr) => {
                self.mbr = self.ram.get_byte(addr);
            }
            MemoryOp::Read(addr) => {
                self.mdr = self.ram.get_word(addr * 4);
            }
            MemoryOp::FetchRead(fetch_addr, read_addr) => {
                self.mbr = self.ram.get_byte(fetch_addr);
                self.mdr = self.ram.get_word(read_addr * 4);
            }
            MemoryOp::None => {}
        }

        match (mir.fetch(), mir.read(), mir.write()) {
            (true, true, false) => {
                self.last_mem_op = MemoryOp::FetchRead(self.pc as u32, self.mar);
            }
            (true, false, _) => {
                self.last_mem_op = MemoryOp::Fetch(self.pc as u32);
            }
            (false, true, false) => {
                self.last_mem_op = MemoryOp::Read(self.mar);
            }
            (_, true, true) => {
                panic!("Undefined Behaviour! RAM cannot be read and written at the same time");
            }
            _ => {}
        }

        let mut addr = mir.next_addr();
        if mir.jmpc() {
            addr |= self.mbr as u16;
        }

        if mir.jamn() {
            addr |= (self.alu.n as u16) << 8;
        }

        if mir.jamz() {
            addr |= (self.alu.z as u16) << 8;
        }

        self.mpc = addr;

        false
    }
}
