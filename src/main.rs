use crate::micro::control_store::ControlStore;
use crate::processor::Mic1;
use iced::{
    button, executor, scrollable, Align, Application, Button, Column, Command, Container, Element,
    Length, Row, Scrollable, Settings, Space, Text,
};

pub mod alu;
pub mod micro;
pub mod processor;
pub mod ram;

#[derive(Debug, Clone, Copy)]
pub enum Message {
    ClockCycle,
    RunProgram,
}

#[derive(Default)]
struct AppState {
    mic1:  Mic1,
    style: style::Style,

    clock_cycle_button: button::State,
    run_program_button: button::State,
    stack_scroll:       scrollable::State,
    program_scroll:     scrollable::State,
}

impl Application for AppState {
    type Executor = executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        let mut mic1 = Mic1::new();

        /*
            BIPUSH 5
            BIPUSH 4
            IADD
        */
        mic1.load_program(vec![0x10, 0x05, 0x10, 0x04, 0x60]);

        (
            AppState {
                mic1,
                ..Default::default()
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Mic1 Emulator - schnavid")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::ClockCycle => {
                self.mic1.clock();
            }
            Message::RunProgram => loop {
                if self.mic1.clock() {
                    break;
                }
            },
        };

        Command::none()
    }

    fn view(&mut self) -> Element<'_, Self::Message> {
        let mut stack = Scrollable::new(&mut self.stack_scroll)
            .spacing(10)
            .padding(10);
        for addr in 0x151..=self.mic1.sp {
            stack = stack.push(
                Container::new(Text::new(format!(
                    "{:#06x}: {} ({1:#x})",
                    addr,
                    self.mic1.ram.get_word(addr as u32 * 4)
                )))
                .padding(5)
                .style(self.style),
            );
        }

        let mut program = Scrollable::new(&mut self.program_scroll)
            .spacing(10)
            .padding(10);
        let mut addr = 0x0;
        while addr != 0x100 {
            program = program.push(
                Container::new(Text::new(format!(
                    "{:#02x}: {:#02x} ({})",
                    addr,
                    self.mic1.ram.get_byte(addr as u32),
                    ControlStore::get_name(self.mic1.ram.get_byte(addr as u32) as u16)
                )))
                .padding(5)
                .style(self.style),
            );
            addr += 1;
        }

        let state = |name, x| {
            Row::with_children(vec![
                Text::new(name).into(),
                Space::with_width(Length::Fill).into(),
                Text::new(x).into(),
            ])
            .into()
        };

        Column::with_children(vec![
            Row::with_children(vec![
                Column::with_children(vec![Text::new("Stack").into(), stack.into()])
                    .align_items(Align::Center)
                    .width(Length::FillPortion(1))
                    .into(),
                Column::with_children(vec![
                    Text::new("Current State").into(),
                    state(
                        "MPC: ",
                        format!(
                            "{:#x} ({})",
                            self.mic1.mpc,
                            ControlStore::get_name(self.mic1.mpc)
                        ),
                    ),
                    Text::new(format!("{}", ControlStore::get_description(self.mic1.mpc))).into(),
                    state("MAR: ", format!("{:#x}", self.mic1.mar)),
                    state("MDR: ", format!("{:#08x}", self.mic1.mdr)),
                    state("PC: ", format!("{:#03x}", self.mic1.pc)),
                    state("MBR: ", format!("{:#08x}", self.mic1.mbr())),
                    state("MBRU: ", format!("{:#08x}", self.mic1.mbru())),
                    state("SP: ", format!("{:#08x}", self.mic1.sp)),
                    state("LV: ", format!("{:#08x}", self.mic1.lv)),
                    state("CPP: ", format!("{:#08x}", self.mic1.cpp)),
                    state("TOS: ", format!("{:#08x}", self.mic1.tos)),
                    state("OPC: ", format!("{:#08x}", self.mic1.opc)),
                    state("H: ", format!("{:#08x}", self.mic1.h)),
                ])
                .width(Length::FillPortion(1))
                .align_items(Align::Center)
                .into(),
                Column::with_children(vec![Text::new("Program").into(), program.into()])
                    .align_items(Align::Center)
                    .width(Length::FillPortion(1))
                    .into(),
            ])
            .height(Length::Fill)
            .spacing(10)
            .into(),
            Row::with_children(vec![
                Button::new(&mut self.clock_cycle_button, Text::new("Clock"))
                    .on_press(Message::ClockCycle)
                    .into(),
                Button::new(&mut self.run_program_button, Text::new("Run Program"))
                    .on_press(Message::RunProgram)
                    .into(),
            ])
            .into(),
        ])
        .padding(10)
        .width(Length::Fill)
        .into()
    }
}

fn main() {
    AppState::run(Settings {
        antialiasing: true,
        ..Settings::default()
    });
}

mod style {
    use iced::container;
    use iced::Color;

    #[derive(Default, Clone, Copy)]
    pub struct Style;

    impl From<Style> for Box<dyn container::StyleSheet> {
        fn from(_: Style) -> Self {
            Container.into()
        }
    }

    const CONTAINER_BG: Color = Color::from_rgb(0.9, 0.9, 0.9);

    pub struct Container;

    impl container::StyleSheet for Container {
        fn style(&self) -> container::Style {
            container::Style {
                background: Some(CONTAINER_BG.into()),
                border_radius: 10,
                ..Default::default()
            }
        }
    }
}
