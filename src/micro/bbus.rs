#[derive(Debug)]
pub enum BBusConfiguration {
    MDR,
    PC,
    MBR,
    MBRU,
    SP,
    LV,
    CPP,
    TOS,
    OPC,
    None,
}

impl From<u8> for BBusConfiguration {
    fn from(i: u8) -> Self {
        match i {
            0 => BBusConfiguration::MDR,
            1 => BBusConfiguration::PC,
            2 => BBusConfiguration::MBR,
            3 => BBusConfiguration::MBRU,
            4 => BBusConfiguration::SP,
            5 => BBusConfiguration::LV,
            6 => BBusConfiguration::CPP,
            7 => BBusConfiguration::TOS,
            8 => BBusConfiguration::OPC,
            _ => BBusConfiguration::None,
        }
    }
}

impl Into<u8> for BBusConfiguration {
    fn into(self) -> u8 {
        match self {
            BBusConfiguration::MDR => 0,
            BBusConfiguration::PC => 1,
            BBusConfiguration::MBR => 2,
            BBusConfiguration::MBRU => 3,
            BBusConfiguration::SP => 4,
            BBusConfiguration::LV => 5,
            BBusConfiguration::CPP => 6,
            BBusConfiguration::TOS => 7,
            BBusConfiguration::OPC => 8,
            BBusConfiguration::None => 15,
        }
    }
}
