use crate::micro::MicroInstruction;
use std::collections::BTreeSet;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum CBusFlag {
    H,
    OPC,
    TOS,
    CPP,
    LV,
    SP,
    PC,
    MDR,
    MAR,
}

pub struct CBusConfiguration(pub BTreeSet<CBusFlag>);

impl<T: AsRef<[u8]> + AsMut<[u8]>> From<&MicroInstruction<T>> for CBusConfiguration {
    fn from(instr: &MicroInstruction<T>) -> Self {
        let mut set = BTreeSet::new();
        if instr.mar() {
            set.insert(CBusFlag::MAR);
        }
        if instr.mdr() {
            set.insert(CBusFlag::MDR);
        }
        if instr.pc() {
            set.insert(CBusFlag::PC);
        }
        if instr.sp() {
            set.insert(CBusFlag::SP);
        }
        if instr.lv() {
            set.insert(CBusFlag::LV);
        }
        if instr.cpp() {
            set.insert(CBusFlag::CPP);
        }
        if instr.tos() {
            set.insert(CBusFlag::TOS);
        }
        if instr.opc() {
            set.insert(CBusFlag::OPC);
        }
        if instr.h() {
            set.insert(CBusFlag::H);
        }
        CBusConfiguration(set)
    }
}

impl CBusConfiguration {
    pub fn new<T: IntoIterator<Item = CBusFlag>>(enables: T) -> CBusConfiguration {
        let mut set = BTreeSet::new();
        for e in enables {
            set.insert(e);
        }
        CBusConfiguration(set)
    }

    pub fn apply<T: AsRef<[u8]> + AsMut<[u8]>>(&self, instr: &mut MicroInstruction<T>) {
        for c in &self.0 {
            match c {
                CBusFlag::H => instr.set_h(true),
                CBusFlag::OPC => instr.set_opc(true),
                CBusFlag::TOS => instr.set_tos(true),
                CBusFlag::CPP => instr.set_cpp(true),
                CBusFlag::LV => instr.set_lv(true),
                CBusFlag::SP => instr.set_sp(true),
                CBusFlag::PC => instr.set_pc(true),
                CBusFlag::MDR => instr.set_mdr(true),
                CBusFlag::MAR => instr.set_mar(true),
            }
        }
    }
}
