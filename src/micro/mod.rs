use crate::alu::ALUConfiguration;
use crate::micro::bbus::BBusConfiguration;
use crate::micro::cbus::CBusConfiguration;
use bitfield::bitfield;

pub mod bbus;
pub mod cbus;
pub mod control_store;

bitfield! {
    #[derive(Copy, Clone)]
    pub struct MicroInstruction(MSB0 [u8]);
    impl Debug;
    u16;
    pub u16, next_addr, set_next_addr: 8, 0;
    pub jmpc, set_jmpc: 9;
    pub jamn, set_jamn: 10;
    pub jamz, set_jamz: 11;
    pub sll8, set_sll8: 12;
    pub sra1, set_sra1: 13;
    pub f0, set_f0: 14;
    pub f1, set_f1: 15;
    pub ena, set_ena: 16;
    pub enb, set_enb: 17;
    pub inva, set_inva: 18;
    pub inc, set_inc: 19;
    pub h, set_h: 20;
    pub opc, set_opc: 21;
    pub tos, set_tos: 22;
    pub cpp, set_cpp: 23;
    pub lv, set_lv: 24;
    pub sp, set_sp: 25;
    pub pc, set_pc: 26;
    pub mdr, set_mdr: 27;
    pub mar, set_mar: 28;
    pub write, set_write: 29;
    pub read, set_read: 30;
    pub fetch, set_fetch: 31;
    pub u8, b, set_b: 35, 32;
}

#[derive(Debug)]
pub struct JAM {
    pub jmpc: bool,
    pub jamn: bool,
    pub jamz: bool,
}

impl JAM {
    pub fn new(jmpc: bool, jamn: bool, jamz: bool) -> JAM {
        JAM { jmpc, jamn, jamz }
    }
}

#[derive(Debug)]
pub struct Mem {
    pub write: bool,
    pub read:  bool,
    pub fetch: bool,
}

impl Mem {
    pub fn new(write: bool, read: bool, fetch: bool) -> Mem {
        Mem { write, read, fetch }
    }
}

impl<T: AsRef<[u8]> + AsMut<[u8]>> MicroInstruction<T> {
    pub fn bbus_config(&self) -> BBusConfiguration {
        self.b().into()
    }

    pub fn set_bbus_config(&mut self, config: BBusConfiguration) {
        self.set_b(config.into())
    }
}

impl MicroInstruction<[u8; 5]> {
    pub fn new(
        next_addr: u16,
        jam: JAM,
        alu: ALUConfiguration,
        c_bus: CBusConfiguration,
        mem: Mem,
        b_bus: BBusConfiguration,
    ) -> Self {
        //                  [ NEXT ADDRESS ]  [     ALU    ]             [  Mem  ]
        //                                [JAM]            [       C      ]     [Bbus]
        let data = [0b0000_0000, 0b0000_0000, 0b0000_0000, 0b0000_0000, 0b0000];

        let mut x = MicroInstruction(data);
        x.set_next_addr(next_addr);
        x.set_jmpc(jam.jmpc);
        x.set_jamn(jam.jamn);
        x.set_jamz(jam.jamz);
        c_bus.apply(&mut x);
        alu.apply(&mut x);
        x.set_write(mem.write);
        x.set_read(mem.read);
        x.set_fetch(mem.fetch);
        x.set_b(b_bus.into());
        x
    }
}

#[test]
fn test_micro_instruction() {
    let data = [0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b0000];

    let x = MicroInstruction(data);
    println!("{:#?}", x);
}
