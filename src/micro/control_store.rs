use crate::alu::{ALUConfiguration, ALUFlag};
use crate::micro::bbus::BBusConfiguration;
use crate::micro::cbus::{CBusConfiguration, CBusFlag};
use crate::micro::{Mem, MicroInstruction, JAM};
use lazy_static::lazy_static;
use std::collections::BTreeMap;

fn make_control_store() -> BTreeMap<u16, MicroInstruction<[u8; 5]>> {
    /*
        ✓ 0x10 BIPUSH byte Push byte onto stack
        0x59 DUP Copy top word on stack and push onto stack
        0xA7 GOTO offset Unconditional branch
        ✓ 0x60 IADD Pop two words from stack; push their sum
        ✓ 0x7E IAND Pop two words from stack; push Boolean AND
        0x99 IFEQ offset Pop word from stack and branch if it is zero
        0x9B IFLT offset Pop word from stack and branch if it is less than zero
        0x9F IF ICMPEQ offset Pop two words from stack; branch if equal
        0x84 IINC varnum const Add a constant to a local variable
        0x15 ILOAD varnum Push local variable onto stack
        0xB6 INVOKEVIRTUAL disp Invoke a method
        ✓ 0x80 IOR Pop two words from stack; push Boolean OR
        0xAC IRETURN Return from method with integer value
        0x36 ISTORE varnum Pop word from stack and store in local variable
        ✓ 0x64 ISUB Pop two words from stack; push their difference
        0x13 LDC W index Push constant from constant pool onto stack
        ✓ 0x00 NOP Do nothing
        0x57 POP Delete word on top of stack
        0x5F SWAP Swap the two top words on the stack
        0xC4 WIDE Prefix instruction; next instruction has a 16-bit index
    */
    let mut m = BTreeMap::new();
    // === Main ===
    m.insert(
        0x100,
        MicroInstruction::new(
            0x0,
            JAM::new(true, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INC]),
            CBusConfiguration::new(vec![CBusFlag::PC]),
            Mem::new(false, false, true),
            BBusConfiguration::PC,
        ),
    );
    // === nop ===
    m.insert(
        0x0,
        MicroInstruction::new(
            0x100,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![]),
            CBusConfiguration::new(vec![]),
            Mem::new(false, false, false),
            BBusConfiguration::None,
        ),
    );
    // === iadd ===
    m.insert(
        // MAR = SP = SP − 1; rd
        0x60,
        MicroInstruction::new(
            0x61,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INVA]),
            CBusConfiguration::new(vec![CBusFlag::SP, CBusFlag::MAR]),
            Mem::new(false, true, false),
            BBusConfiguration::SP,
        ),
    );
    m.insert(
        // H = TOS
        0x61,
        MicroInstruction::new(
            0x62,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::F1, ALUFlag::ENB]),
            CBusConfiguration::new(vec![CBusFlag::H]),
            Mem::new(false, false, false),
            BBusConfiguration::TOS,
        ),
    );
    m.insert(
        // MDR = TOS = MDR + H; wr; goto Main1
        0x62,
        MicroInstruction::new(
            0x100,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::ENA, ALUFlag::F0, ALUFlag::F1]),
            CBusConfiguration::new(vec![CBusFlag::MDR, CBusFlag::TOS]),
            Mem::new(true, false, false),
            BBusConfiguration::MDR,
        ),
    );
    // === isub ===
    m.insert(
        // MAR = SP = SP − 1; rd
        0x64,
        MicroInstruction::new(
            0x65,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INVA]),
            CBusConfiguration::new(vec![CBusFlag::SP, CBusFlag::MAR]),
            Mem::new(false, true, false),
            BBusConfiguration::SP,
        ),
    );
    m.insert(
        // H = TOS
        0x65,
        MicroInstruction::new(
            0x66,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::F1, ALUFlag::ENB]),
            CBusConfiguration::new(vec![CBusFlag::H]),
            Mem::new(false, false, false),
            BBusConfiguration::TOS,
        ),
    );
    m.insert(
        // MDR = TOS = MDR - H; wr; goto Main1
        0x66,
        MicroInstruction::new(
            0x100,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![
                ALUFlag::ENB,
                ALUFlag::ENA,
                ALUFlag::F0,
                ALUFlag::F1,
                ALUFlag::INVA,
                ALUFlag::INC,
            ]),
            CBusConfiguration::new(vec![CBusFlag::MDR, CBusFlag::TOS]),
            Mem::new(true, false, false),
            BBusConfiguration::MDR,
        ),
    );
    // === iand ===
    m.insert(
        // MAR = SP = SP − 1; rd
        0x7E,
        MicroInstruction::new(
            0x7F,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INVA]),
            CBusConfiguration::new(vec![CBusFlag::SP, CBusFlag::MAR]),
            Mem::new(false, true, false),
            BBusConfiguration::SP,
        ),
    );
    m.insert(
        // H = TOS
        0x7F,
        MicroInstruction::new(
            0x180,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::F1, ALUFlag::ENB]),
            CBusConfiguration::new(vec![CBusFlag::H]),
            Mem::new(false, false, false),
            BBusConfiguration::TOS,
        ),
    );
    m.insert(
        // MDR = TOS = MDR AND H; wr; goto Main1
        0x180,
        MicroInstruction::new(
            0x100,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::ENA]),
            CBusConfiguration::new(vec![CBusFlag::MDR, CBusFlag::TOS]),
            Mem::new(true, false, false),
            BBusConfiguration::MDR,
        ),
    );
    // === ior ===
    m.insert(
        // MAR = SP = SP − 1; rd
        0x80,
        MicroInstruction::new(
            0x81,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INVA]),
            CBusConfiguration::new(vec![CBusFlag::SP, CBusFlag::MAR]),
            Mem::new(false, true, false),
            BBusConfiguration::SP,
        ),
    );
    m.insert(
        // H = TOS
        0x81,
        MicroInstruction::new(
            0x82,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::F1, ALUFlag::ENB]),
            CBusConfiguration::new(vec![CBusFlag::H]),
            Mem::new(false, false, false),
            BBusConfiguration::TOS,
        ),
    );
    m.insert(
        // MDR = TOS = MDR + H; wr; goto Main1
        0x82,
        MicroInstruction::new(
            0x100,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::ENA, ALUFlag::F1]),
            CBusConfiguration::new(vec![CBusFlag::MDR, CBusFlag::TOS]),
            Mem::new(true, false, false),
            BBusConfiguration::MDR,
        ),
    );
    // === bipush ===
    m.insert(
        // SP = MAR = SP + 1
        0x10,
        MicroInstruction::new(
            0x11,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INC]),
            CBusConfiguration::new(vec![CBusFlag::SP, CBusFlag::MAR]),
            Mem::new(false, false, false),
            BBusConfiguration::SP,
        ),
    );
    m.insert(
        // PC = PC + 1; fetch
        0x11,
        MicroInstruction::new(
            0x12,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F0, ALUFlag::F1, ALUFlag::INC]),
            CBusConfiguration::new(vec![CBusFlag::PC]),
            Mem::new(false, false, true),
            BBusConfiguration::PC,
        ),
    );
    m.insert(
        // MDR = TOS = MBR; wr; goto Main1
        0x12,
        MicroInstruction::new(
            0x100,
            JAM::new(false, false, false),
            ALUConfiguration::new(vec![ALUFlag::ENB, ALUFlag::F1]),
            CBusConfiguration::new(vec![CBusFlag::MDR, CBusFlag::TOS]),
            Mem::new(true, false, true),
            BBusConfiguration::MBR,
        ),
    );
    m
}

pub struct ControlStore(BTreeMap<u16, MicroInstruction<[u8; 5]>>);

impl Default for ControlStore {
    fn default() -> Self {
        ControlStore(make_control_store())
    }
}

impl ControlStore {
    #[inline]
    pub fn get(&self, addr: u16) -> MicroInstruction<[u8; 5]> {
        self.0
            .get(&addr)
            .copied()
            .unwrap_or_else(|| self.0.get(&0x000).copied().unwrap())
    }

    #[inline]
    pub fn get_instr(addr: u16) -> MicroInstruction<[u8; 5]> {
        CONTROL_STORE.get(addr)
    }

    #[inline]
    pub fn get_name(addr: u16) -> String {
        MIR_NAMES.get(&addr).cloned().unwrap_or("".to_owned())
    }

    #[inline]
    pub fn get_description(addr: u16) -> String {
        MIR_DESCRIPTIONS
            .get(&addr)
            .cloned()
            .unwrap_or("".to_owned())
    }
}

fn make_mir_names() -> BTreeMap<u16, String> {
    let mut m = BTreeMap::new();
    m.insert(0x0, "nop".to_owned());
    m.insert(0x10, "bipush1".to_owned());
    m.insert(0x11, "bipush2".to_owned());
    m.insert(0x12, "bipush3".to_owned());
    m.insert(0x60, "iadd1".to_owned());
    m.insert(0x61, "iadd2".to_owned());
    m.insert(0x62, "iadd3".to_owned());
    m.insert(0x64, "isub1".to_owned());
    m.insert(0x65, "isub2".to_owned());
    m.insert(0x66, "isub3".to_owned());
    m.insert(0x7E, "iand1".to_owned());
    m.insert(0x7F, "iand2".to_owned());
    m.insert(0x180, "iand3".to_owned());
    m.insert(0x80, "ior1".to_owned());
    m.insert(0x81, "ior2".to_owned());
    m.insert(0x82, "ior3".to_owned());
    m.insert(0x100, "Main".to_owned());
    m
}

fn make_mir_descriptions() -> BTreeMap<u16, String> {
    let mut m = BTreeMap::new();
    m.insert(0x0, "goto Main".to_owned());
    m.insert(0x10, "SP = MAR = SP + 1".to_owned());
    m.insert(0x11, "PC = PC + 1; fetch".to_owned());
    m.insert(0x12, "MDR = TOS = MBR; wr; goto Main".to_owned());
    m.insert(0x60, "MAR = SP = SP - 1; rd".to_owned());
    m.insert(0x61, "H = TOS".to_owned());
    m.insert(0x62, "MDR = TOS = MDR + H; wr; goto Main".to_owned());
    m.insert(0x64, "MAR = SP = SP - 1; rd".to_owned());
    m.insert(0x65, "H = TOS".to_owned());
    m.insert(0x66, "MDR = TOS = MDR - H; wr; goto Main".to_owned());
    m.insert(0x7E, "MAR = SP = SP - 1".to_owned());
    m.insert(0x7F, "H = TOS".to_owned());
    m.insert(0x180, "MDR = TOS = MDR AND H; wr; goto Main".to_owned());
    m.insert(0x80, "MAR = SP = SP - 1; rd".to_owned());
    m.insert(0x81, "H = TOS".to_owned());
    m.insert(0x82, "MDR = TOS = MDR OR H; wr; goto Main".to_owned());
    m.insert(0x100, "PC = PC + 1; fetch; goto (MBR)".to_owned());
    m
}

lazy_static! {
    pub static ref CONTROL_STORE: ControlStore = Default::default();
    pub static ref MIR_NAMES: BTreeMap<u16, String> = make_mir_names();
    pub static ref MIR_DESCRIPTIONS: BTreeMap<u16, String> = make_mir_descriptions();
}
