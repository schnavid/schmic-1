use crate::micro::MicroInstruction;
use std::collections::BTreeSet;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum ALUFlag {
    SLL8,
    SRA1,
    F0,
    F1,
    ENA,
    ENB,
    INVA,
    INC,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum ALUAction {
    AAndB,
    AOrB,
    NotB,
    APlusB,
}

#[derive(Debug)]
pub struct ALUConfiguration(BTreeSet<ALUFlag>);

impl<T: AsRef<[u8]> + AsMut<[u8]>> From<&MicroInstruction<T>> for ALUConfiguration {
    fn from(instr: &MicroInstruction<T>) -> Self {
        let mut set = BTreeSet::new();
        if instr.sll8() {
            set.insert(ALUFlag::SLL8);
        }
        if instr.sra1() {
            set.insert(ALUFlag::SRA1);
        }
        if instr.f0() {
            set.insert(ALUFlag::F0);
        }
        if instr.f1() {
            set.insert(ALUFlag::F1);
        }
        if instr.ena() {
            set.insert(ALUFlag::ENA);
        }
        if instr.enb() {
            set.insert(ALUFlag::ENB);
        }
        if instr.inva() {
            set.insert(ALUFlag::INVA);
        }
        if instr.inc() {
            set.insert(ALUFlag::INC);
        }
        ALUConfiguration(set)
    }
}

impl ALUConfiguration {
    pub fn new<T: IntoIterator<Item = ALUFlag>>(enables: T) -> ALUConfiguration {
        let mut set = BTreeSet::new();
        for e in enables {
            set.insert(e);
        }
        ALUConfiguration(set)
    }

    pub fn apply<T: AsRef<[u8]> + AsMut<[u8]>>(&self, instr: &mut MicroInstruction<T>) {
        for e in &self.0 {
            match e {
                ALUFlag::SLL8 => instr.set_sll8(true),
                ALUFlag::SRA1 => instr.set_sra1(true),
                ALUFlag::F0 => instr.set_f0(true),
                ALUFlag::F1 => instr.set_f1(true),
                ALUFlag::ENA => instr.set_ena(true),
                ALUFlag::ENB => instr.set_enb(true),
                ALUFlag::INVA => instr.set_inva(true),
                ALUFlag::INC => instr.set_inc(true),
            }
        }
    }

    #[inline]
    pub fn is_set(&self, flag: ALUFlag) -> bool {
        self.0.contains(&flag)
    }

    pub fn get_action(&self) -> ALUAction {
        let f0 = self.is_set(ALUFlag::F0);
        let f1 = self.is_set(ALUFlag::F1);

        match (f0, f1) {
            (false, false) => ALUAction::AAndB,
            (false, true) => ALUAction::AOrB,
            (true, false) => ALUAction::NotB,
            (true, true) => ALUAction::APlusB,
        }
    }
}

#[derive(Default, Debug)]
pub struct ALU {
    pub n: bool,
    pub z: bool,
}

impl ALU {
    pub fn calculate(&mut self, config: ALUConfiguration, h: i32, b: i32) -> i32 {
        // Input Flags
        let a = if config.is_set(ALUFlag::ENA) { h } else { 0x0 };
        let a = if config.is_set(ALUFlag::INVA) { !a } else { a };
        let b = if config.is_set(ALUFlag::ENB) { b } else { 0x0 };

        // Main ALU
        let result = match config.get_action() {
            ALUAction::AAndB => a & b,
            ALUAction::AOrB => a | b,
            ALUAction::NotB => !b,
            ALUAction::APlusB => {
                let result = a + b;
                if config.is_set(ALUFlag::INC) {
                    result + 1
                } else {
                    result
                }
            }
        };

        // Shifter
        let result = if config.is_set(ALUFlag::SLL8) {
            result << 8
        } else {
            result
        };
        let result = if config.is_set(ALUFlag::SRA1) {
            result >> 1
        } else {
            result
        };

        self.z = result == 0;
        self.n = result < 0;

        result
    }
}
